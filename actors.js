import * as config from './module/config.js';
import * as actorEditor from './module/actorEdition.js';
import * as links from './module/links.js';

Hooks.once("init", () => {
  console.log('WA-Anvil-Actors | Initializing World Anvil Actors Module');
  config.registerSettings();
});

/**
 * Adding a isCharacter flag to each entry
 */
 Hooks.on("WAParseArticle", (article, result) => {
  result.waFlags.isCharacter = article.template === 'person';
});


Hooks.on("createJournalEntry", (entry, options, user) => {

  if( !entry.getFlag("world-anvil", "isCharacter") ) return;
  if( game.user.id != user ) { return; }

  // Create related actor
  actorEditor.createOrUpdateRelatedCharacter( entry );
});

Hooks.on("updateJournalEntry", (entry, data, options, user) => {
  if( !entry.getFlag("world-anvil", "isCharacter") ) return;
  if( game.user.id != user ) { return; }

  // Create related actor
  actorEditor.createOrUpdateRelatedCharacter( entry );
});

/**
 * Adding actor link inside each journal sheet
 */
Hooks.on("renderJournalSheet", (app, html, data) => {
  const entry = app.object;
  if( !entry.getFlag("world-anvil", "isCharacter") ) { return; }

  links.addActorLinkInsideJournalEntry(entry, html);
});


/**
 * Adding journal link inside each actor sheet
 */
 Hooks.on("renderActorSheet", (app, html, data) => {
  const actor = app.object;
  if( !actor.getFlag("world-anvil", "isCharacter") ) { return; }

  links.addJournalLinkInsideActorSheet(actor, html);
  links.bindWaLinks(html);
});

/**
 * Adding a character icon on each line related to an actor
 */
Hooks.on("renderWorldAnvilBrowser", (app, html, data) => {

  links.addActorButtonsInBrower(html);
  links.addOpenActorSheetListeners(html);
});
