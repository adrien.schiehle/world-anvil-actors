export function registerSettings() {

    game.settings.register("world-anvil-actors", "characterType", {
        name: "WAActors.characterTypeLabel",
        hint: "WAActors.characterTypeHint",
        scope: "world",
        type: String,
        default: 'npc',
        config: true
    });

    game.settings.register("world-anvil-actors", "characterBio", {
        name: "WAActors.characterBioLabel",
        hint: "WAActors.characterBioHint",
        scope: "world",
        type: String,
        default: 'details.biography.value',
        config: true
    });

    game.settings.register("world-anvil-actors", "characterToken", {
        name: "WAActors.characterTokenLabel",
        hint: "WAActors.characterTokenHint",
        scope: "world",
        type: String,
        default: 'icons/svg/mystery-man.svg',
        config: true
    });
}
