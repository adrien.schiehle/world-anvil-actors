/**
 * Adding actor link inside journal Entry
 * Only available if the entry is related to a WA character
 * @param {JournalEntry} journalEntry Opened journal entry
 * @param {HTMLElement} html Journal sheet content
 */
export function addActorLinkInsideJournalEntry(journalEntry, html) {

  const articleId = journalEntry.getFlag("world-anvil", "articleId");
  const actor = game.actors.contents.find(a => a.getFlag('world-anvil', 'articleId')  === articleId );
  if( !actor ) { return; } // Meaning actor as been manually removed (or imported before this module addition)

  const link = $(`<a class="wa-sync"><i class="fas fa-user"></i>${game.i18n.localize("WAActors.actorLink")}</a>`);
  link.on("click", event => {
    event.preventDefault();
    actor.sheet.render(true);
    journalEntry.sheet.close();
  });

  const title = html.find(".window-title");
  if( title ) {
    title.after(link);
  }
}

/**
 * Adding journal link inside each actor sheet
 * Only available if the actor is a WA character
 * @param {Actor} actor Opened actor
 * @param {HTMLElement} html Journal sheet content
 */
 export function addJournalLinkInsideActorSheet(actor, html) {

  const articleId = actor.getFlag("world-anvil", "articleId");
  const entry = game.journal.find(a => a.getFlag('world-anvil', 'articleId')  === articleId );
  if( !entry ) { return; } // Meaning journal entry as been manually removed

  const link = $(`<a class="wa-sync"><i class="fas fa-feather"></i>${game.i18n.localize("WAActors.journalLink")}</a>`);
  link.on("click", event => {
    event.preventDefault();
    entry.sheet.render(true);
    actor.sheet.close();
  });

  const title = html.find(".window-title");
  if( title ) {
    title.after(link);
  }
}
  
/**
 * Add an Actor button on each article line corresponding to a WA Character article
 * @param {HTMLElement} html WorldAnvilBrowser sheet content
 */
export function addActorButtonsInBrower(html) {

  const articles = html.find(".article");
  for( let index = 0; index < articles.length; index++ ) {
    const article = articles[index];
    const articleId = article.dataset.articleId;
    const entryId = article.dataset.entryId;
    if( !articleId || !entryId ) { continue; } // Not yet imported

    const entry = game.journal.get(entryId);
    const isCharacter = entry?.getFlag("world-anvil", "isCharacter") ?? false;
    if( !isCharacter ) { continue; } // Not a WA Character article

    
    // Creating additional button for the Actor
    const actorButton = document.createElement("button");
    actorButton.type = "button";
    actorButton.innerHTML = '<i class="fas fa-user"></i>';
    actorButton.classList.add("world-anvil-control", "open-actor");

    const actor = game.actors.contents.find(a => a.getFlag('world-anvil', 'articleId')  === articleId );
    if( actor ) {
      actorButton.title = game.i18n.localize("WAActors.buttonOpenActorSheet");
      actorButton.dataset["actorId"] = actor.id;
    } else {
      actorButton.title = game.i18n.localize("WAActors.buttonNoActorSheet");
      actorButton.classList.add("inactive");
    }

    // Add it to the control line
    const controls = article.getElementsByClassName("controls")[0];
    controls.prepend(actorButton);
    actorButton.outerHTML += '\n';
  }
}

/**
 * Add listeners to previously added buttons
 * @param {HTMLElement} html WorldAnvilBrowser sheet content
 */
export function addOpenActorSheetListeners(html) {

  html.find(".world-anvil-control.open-actor").click( event => { // Look for previously added buttons
    event.preventDefault();
    const actorId = event.currentTarget.dataset.actorId;
    if( !actorId ) { return; } // Meaning, actor has not yet be created

    const actor = game.actors.get(actorId);
    actor?.sheet.render(true);
  });
}

/**
 * Binding WA Links so that they can also be used inside ActorSheets
 * @param {*} html 
 */
export function bindWaLinks(html) {

  // Activate cross-link listeners
  html.find(".wa-link").click(event => {
    event.preventDefault();
    const articleId = event.currentTarget.dataset.articleId;

    // View an existing linked article (OBSERVER+)
    const entry = game.journal.find(e => e.getFlag("world-anvil", "articleId") === articleId);
    if ( entry ) {
      if ( !entry.testUserPermission(game.user, "OBSERVER") ) {
        return ui.notifications.warn(game.i18n.localize("WA.NoPermissionView"));
      }
      return entry.sheet.render(true);
    }

    // Import a new article (GM Only)
    if ( !game.user.isGM ) {
      return ui.notifications.warn(game.i18n.localize("WA.NoPermissionView"));
    }
    const coreModule = game.modules.get("world-anvil");
    return coreModule.api.importArticle(articleId, {renderSheet: true});
  });

}