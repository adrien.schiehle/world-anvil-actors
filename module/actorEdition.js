
/** Ref to world anvil module api */
let worldAnvilApi = null;

/**
 * Create / update related character for entries related to WA characters
 * @param {JournalEntry} journalEntry The entry which has just be created or updated
 */
export async function createOrUpdateRelatedCharacter(journalEntry) {

    // Retrieve API
    if( !worldAnvilApi ) { worldAnvilApi = game.modules.get("world-anvil").api; }

    const articleId = journalEntry.getFlag("world-anvil", "articleId");
    const relatedCharacters = game.actors.contents.filter(a => a.getFlag('world-anvil', 'articleId')  === articleId );

    // Update current ones if some are present
    if( relatedCharacters.length > 0 ) {
        for( let actor of relatedCharacters ) {
            await updateExistingActor(actor, journalEntry);
        }

    } else { // Or create a new one
        await createNewActor(journalEntry);
    }
}


/** Update an existing character with the current journal entry content */
async function updateExistingActor(actor, journalEntry) {

    const data = {
        name: journalEntry.name,
        "flags.world-anvil": journalEntry.flags["world-anvil"]
    };

    const img = getActorPortraitFromEntry(journalEntry);
    if( img ) { data.img = img; }

    const bio = getActorBiographyFromEntry(journalEntry);
    const defaultField = game.settings.get("world-anvil-actors", "characterBio");
    data['system.' + defaultField] = bio;
    addActorPermissions(journalEntry, data);

    await actor.update(data);
    console.info(`World-Anvil Actors | Character ${actor.name} has been updated`);
}


/** Create a new actor from the journalEntry */
async function createNewActor(journalEntry) {

    const categories = ( await worldAnvilApi.getCategories() ).categories;
    associateCategoryActorFolders(categories);

    const categoryId = journalEntry.folder?.getFlag("world-anvil", "categoryId") ?? null;
    const category = categories.get(categoryId);
    const folder = await getCategoryActorFolder(category);

    const defaultType = game.settings.get("world-anvil-actors", "characterType");
    const defaultField = game.settings.get("world-anvil-actors", "characterBio");
    const defaultToken = game.settings.get("world-anvil-actors", "characterToken");

    const data = {
        type: defaultType,
        name: journalEntry.name,
        folder: folder?.id ?? null,
        token: {
            img: defaultToken,
            displayName: 30,
            disposition: 0,
            actorLink: true
        },
        "flags.world-anvil": journalEntry.flags["world-anvil"]
    };

    const img = getActorPortraitFromEntry(journalEntry);
    if( img ) { data.img = img; }

    const bio = getActorBiographyFromEntry(journalEntry);
    data['system.' + defaultField] = bio;
    addActorPermissions(journalEntry, data);
    
    const actor = await Actor.create(data);
    console.info(`World-Anvil Actors | Character ${actor.name} has been created`);
}

/**
 * Used while creating or updating Actor.
 * Retrieve data from some Journal Entry page to choose the right image
 * @param {JournalEntry} journalEntry 
 * @returns Image url
 */
function getActorPortraitFromEntry(journalEntry) {

    const pageNames = journalEntry.getFlag("world-anvil", "pageNames");

    const imageHeaders = [pageNames.portrait, pageNames.image, pageNames.cover ];
    const imagePages = Array.from(journalEntry.pages.values()).filter( p => imageHeaders.includes(p.name) );
    imagePages.sort( (a,b) => imageHeaders.indexOf(a.name) - imageHeaders.indexOf(b.name));

    return imagePages[0]?.src;
}

/**
 * Used while creating or updating Actor.
 * Append main article, sideContent, relations, and secrets in one big page
 * @param {JournalEntry} journalEntry 
 * @returns {string} innerHTML which will be used a biography
 */
 function getActorBiographyFromEntry(journalEntry) {

    const pageNames = journalEntry.getFlag("world-anvil", "pageNames");
    const textHeaders = [pageNames.mainArticle, pageNames.sideContent, pageNames.relations, pageNames.secrets ];
    const textPages = Array.from(journalEntry.pages.values()).filter( p => textHeaders.includes(p.name) );
    textPages.sort( (a,b) => textHeaders.indexOf(a.name) - textHeaders.indexOf(b.name));

    return textPages.reduce( (_result, current) => {
        return _result + (_result=="" ? "" : "<hr/>") + current.text.content;
    }, "");
}


/**
 * Same as the getCategoryFolder from the core world anvil module.
 * Except it looks inside Actor folder instead of JournalEntry ones.
 * @param {Category} category         The category of interest
 * @returns {Promise<Folder>}         The Folder document which contains entries in this category
 */
async function getCategoryActorFolder(category) {
    if( !category ) return null;
    if ( category.actorFolder !== undefined ) return category.actorFolder;
    if ( category.parent && !category.parent.actorFolder ) await getCategoryActorFolder(category.parent);
  
    // Check whether a Folder already exists for this Category
    const folder = game.folders.find(f => (f.type === "Actor") && (f.getFlag("world-anvil", "categoryId") === category.id) );
    if ( folder ) return category.actorFolder = folder;
  
    // Create a new Folder
    return category.actorFolder = await Folder.create({
      name: `[WA] ${category.title}`,
      type: 'Actor',
      parent: category.parent?.actorFolder?.id,
      sorting: 'm',
      "flags.world-anvil.categoryId": category.id
    });
}

/**
 * Check JournalEntry permissions, and adjustor related actor permissions to match it.
 * @param {JournalEntry} journalEntry The created or updated journal Entry
 * @param {object} data data used for creating or updating actor
 */
function addActorPermissions(journalEntry, data) {
    if( journalEntry.ownership.default < CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER ) {
        data['permission.default'] = CONST.DOCUMENT_PERMISSION_LEVELS.NONE;
    } else {
        data['permission.default'] = CONST.DOCUMENT_PERMISSION_LEVELS.LIMITED;
    }
}

/**
 * Associated category.actorFolder from the WA hierarchy with existing Folders within the World.
 * @param {CategoryMap} categories      The categories being mapped
 */
function associateCategoryActorFolders(categories) {
    const folders = game.folders.filter(f => (f.type === "Actor") && f.flags["world-anvil"]);
    for ( let [id, category] of categories ) {
      if ( id === worldAnvilApi.CATEGORY_ID.root ) category.actorFolder = null;
      else category.actorFolder = folders.find(f => f.getFlag("world-anvil", "categoryId") === id);
    }
}