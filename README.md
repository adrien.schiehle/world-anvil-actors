# Foundry Virtual Tabletop - World Anvil Integration - Automatic character creation

This module allow Game Masters to automatically create Actors inside Foundry when importing a character article from [World Anvil](https://worldanvil.com).

It comes on top of the core module [World Anvil Integration](https://gitlab.com/foundrynet/world-anvil/-/raw/master/module.json)

-----

## Installation

This module can be installed by using the following module manifest url: (https://gitlab.com/adrien.schiehle/world-anvil-actors/-/raw/main/module.json).

-----

## Configuration

Three things can be configured when automatically creating actors :
- Which actor type should be used
- On which field should the journal content be put ( => biography )
- Which defaut token should be used for the actor

The default Actor type and content field are compatible with Pathfinder 1 system.

![Configuration panel](docs/README_config_panel.png?raw=true)

### Where do I find the correct values needed for my Game system ?

You will need to dwelve inside the `template.json` of your game system to find those values.

In this example, correct values are :
- Actor type : character
- Field for biography : biography
![Setting configuration](docs/README_config_actor_type.png?raw=true)

-----

## Feature

### What is automatically set

When importing a new character article, an actor is created with default values, except : 
- Its name
- The actor image
- The actor biography
- A defaut image is set for the actor token

On later update, all actor editions are kept except :
- Its name
- The actor image
- The actor biography

> You can change the token image after the first creation. It won't be overriden.

### Changes on Journal Entries and Actor Sheet

You can directly navigate between the Journal Entry and the Actor sheet of a character via header buttons :

![Navigate through sheets](docs/README_sheet_links.png?raw=true)

You can also directly open the Actor sheet from the WorldAnvil browser :

![WorldAnvil Browser](docs/README_wabrowser.png?raw=true)

-----

## Common questions

### Where are my Actors created

On first import, it will create a folder hierarchy corresponding to your article categories. You will thn have a similar folder structure in your Journals tab and your Actors tab.

I you decide to move your Actor elsewhere after that, the link will remain. And futher updates of your article with still be transfered to the Actor biography.

### I'm also using world-anvil-secrets. What happens to those ?

If you're also using the custom secret module, secret will be displayed in the biography the same way it is displayed on the Journal entry.

Game Masters will have the same level of functionnality to toggle the secrets directly via the Actor sheet.

### Why do I need a default token for my actors ?

#### A technical issue
When retrieving articles from [World Anvil](https://worldanvil.com), images are not downloaded from obvious reasons.

Journal Entries links to World Anvil website when it need to display a picture.

This method also works for Actor main image and sheet. But it doesn't work for tokens. A CORS problem remain. So for now it's impossible.

-----
